import './App.css';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ProductView from './pages/ProductView';
import AddProduct from './pages/AddProduct';
import Error from './pages/Error';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    const token = localStorage.getItem('token');
    console.log(user);

    if (token) {

      fetch(`http://localhost:4000/users/details`, {
        headers: { Authorization: `Bearer ${token}` }
      }).then(res => res.json()).then(data => {
        console.log(data)
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
    }
  }, [])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/product/" element={<Products />} />
            <Route path="/product/:productId" element={<ProductView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider >
  );
}

export default App;
