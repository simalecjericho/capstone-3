import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
    const { productId } = useParams();
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);
    const [totalPrice, setTotalPrice] = useState(price);

    const calculateTotalPrice = () => {
        const totalPrice = quantity * price;
        setTotalPrice(totalPrice);
    };

    const buy = (productId) => {
        fetch(`http://localhost:4000/users/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity,
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data.message) {
                    Swal.fire({
                        title: "Successfully bought",
                        icon: 'success',
                        text: "You have successfully bought this product."
                    });

                    navigate("/product/");
                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    });
                }
            });
    };

    useEffect(() => {
        console.log(productId);
        fetch(`http://localhost:4000/product/${productId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setImageUrl(data.imageUrl);
            });
    }, [productId]);

    useEffect(() => {
        calculateTotalPrice();
    }, [quantity, price]);

    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <div className="text-center">
                            <img
                                src={`/${imageUrl}`}
                                alt={name}
                                style={{ maxWidth: '100%', maxHeight: '200px', width: 'auto', height: 'auto' }}
                            />
                        </div>
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Form.Group>
                                <Form.Label>Quantity:</Form.Label>
                                <div className="input-group">
                                    <Button
                                        variant="outline-primary"
                                        onClick={() => {
                                            setQuantity(quantity > 1 ? quantity - 1 : 1);
                                            calculateTotalPrice();
                                        }}
                                    >
                                        -
                                    </Button>
                                    <Form.Control
                                        type="number"
                                        value={quantity}
                                        onChange={(e) => {
                                            setQuantity(e.target.value);
                                            calculateTotalPrice();
                                        }}
                                    />
                                    <Button
                                        variant="outline-primary"
                                        onClick={() => {
                                            setQuantity(quantity + 1);
                                            calculateTotalPrice();
                                        }}
                                    >
                                        +
                                    </Button>
                                </div>
                            </Form.Group>
                            <Card.Subtitle className='mt-2'>Total Price:</Card.Subtitle>
                            <Card.Text>PhP {totalPrice}</Card.Text>
                            {user.id !== null ? (
                                <Button
                                    variant="primary"
                                    block={true.toString()}
                                    onClick={() => buy(productId)}
                                >
                                    Buy now!
                                </Button>
                            ) : (
                                <Link className="btn btn-danger btn-block" to="/login">
                                    Log in to purchase
                                </Link>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
