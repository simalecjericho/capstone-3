import Banner from '../components/Banner';
import FeaturedProducts from '../components/FeaturedProduct';

export default function Home() {
    const data = {
        title: "Zuitt Book Store",
        content: "Quality books for everyone, everywhere",
        destination: "/product/",
        label: "Buy now!"
    }

    return (
        <div className="home-container">
            <Banner data={data} />
            <div className="featured-products-container">
                <FeaturedProducts />
            </div>
            <style jsx>{`
                .home-container {
                    background-color: #f0f0f0;
                    padding: 20px;
                }
                
                .featured-products-container {
                    background-color: #fff;
                    border-radius: 5px;
                    padding: 20px;
                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                }

                h2 {
                    margin-bottom: 20px;
                }
            `}</style>
        </div>
    )
}
