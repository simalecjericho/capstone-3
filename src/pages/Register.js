import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register() {
    const { user } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);
    console.log(confirmPassword);

    function isPasswordValid(password) {
        const passwordRegex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;
        return passwordRegex.test(password);
    }

    function registerUser(e) {
        e.preventDefault();

        if (!isPasswordValid(password)) {
            alert("Password must be at least 6 characters and contain 1 uppercase letter, 1 number, and 1 special character.");
            return;
        }

        fetch('http://localhost:4000/users/register', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data) {
                    setEmail("");
                    setPassword("");
                    setConfirmPassword("")

                    alert("Thank you for registering")
                } else {
                    alert("Please try again later")
                }
            })

    }

    function togglePasswordVisibility(e) {
        e.preventDefault();
        setShowPassword(!showPassword);
    }

    useEffect(() => {
        if (email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password, confirmPassword])


    return (
        (user.id !== null) ?
            <Navigate to="/product/" />
            :
            <Form onSubmit={(e) => registerUser(e)}>
                <h1 className="my-5 text-center">Register</h1>
                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => { setEmail(e.target.value) }} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <div className="password-input">
                        <Form.Control type={showPassword ? "text" : "password"} placeholder="Enter Password" required value={password} onChange={e => { setPassword(e.target.value) }} />
                        <Button
                            variant="secondary"
                            type="button"
                            onClick={togglePasswordVisibility}
                        >
                            {showPassword ? "Hide" : "Show"}
                        </Button>
                    </div>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Confirm Password:</Form.Label>
                    <Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => { setConfirmPassword(e.target.value) }} />
                    {!isPasswordValid(password) && (
                        <div className="text-danger">Password must be at least 6 characters and contain 1 uppercase letter, 1 number, and 1 special character.</div>
                    )}
                </Form.Group>
                {
                    isActive

                        ? <Button variant="primary" type="submit" id="submitBtn" >Submit</Button>
                        : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                }
            </Form>
    )
}