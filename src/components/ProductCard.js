import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({ productProp }) {
    const { imageUrl, _id, name, description, price } = productProp;

    return (
        <Card className="mt-3">
            <Card.Body>
                <div style={{ width: '100px', height: '100px' }}>
                    <img src={`/${imageUrl}`} style={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }} />
                </div>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    );
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
