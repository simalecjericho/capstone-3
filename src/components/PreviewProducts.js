import React from 'react';
import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product(props) {
    const { breakPoint, data } = props;
    const { _id, name, description, price, imageUrl } = data;

    return (
        <Col xs={12} md={breakPoint}>
            <Card className="product-card mx-2">
                <Card.Body style={{ minHeight: '200px' }}>
                    <div className="text-center">
                        <div style={{ width: '100px', height: '100px' }}>
                            <img
                                src={`/${imageUrl}`}
                                style={{ maxWidth: '100%', maxHeight: '100%', width: 'auto', height: 'auto' }}
                                alt={name}
                            />
                        </div>
                        <Link to={`/product/${_id}`}>{name}</Link>
                    </div>
                    <Card.Text>{description}</Card.Text>
                </Card.Body>
                <Card.Footer>
                    <h5 className="text-center">₱{price}</h5>
                    <Link className="btn btn-primary d-block" to={`/product/${_id}`}>
                        Details
                    </Link>
                </Card.Footer>
            </Card>
        </Col>
    );
}
