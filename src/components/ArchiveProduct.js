import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({ product, isActive, fetchData }) {

    const archiveToggle = (productId) => {
        fetch(`http://localhost:4000/product/${productId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data) {
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully disabled'
                    })
                    fetchData();

                } else {
                    Swal.fire({
                        title: 'Something Went Wrong',
                        icon: 'error',
                        text: 'Please Try again'
                    })
                    fetchData();
                }
            })
    }

    const activateToggle = (productId) => {
        const token = localStorage.getItem('token')
        console.log(token)
        fetch(`http://localhost:4000/product/${productId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data) {
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully enabled'
                    })
                    fetchData();
                } else {
                    console.log("Archive Response Data:", data);
                    Swal.fire({
                        title: 'Something Went Wrong',
                        icon: 'error',
                        text: 'Please Try again'
                    })
                    fetchData();
                }
            })
    }

    return (
        <>
            {isActive
                ?
                <Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>
                :
                <Button variant="success" size="sm" onClick={() => activateToggle(product)}>Activate</Button>
            }
        </>
    )
}
