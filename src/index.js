import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';

// createRoot - displays the element to be managed by React with its virual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));

// render() - displays the react elements/components into the root
// App component is our mother component, this is the component we use as entry point and where we can render all other components or pages
// React.strictmode - compoenent from React that manages future possible conflicts
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

